<?php

return [
    'alert_force_password_change' => 'You need to change your password',
    'all_rights_reserved' => 'All rights reserved',
    'all_users' => 'All users',
    'configure_your_system' => 'Configure your system',
    'control_you_working_hours' => 'Control you working hours',
    'copyright' => 'Copyright',
    'create_user' => 'Create user',
    'edit_user' => 'Edit user',
    'entrance' => 'Entrance',
    'enter_administrative_panel' => 'Enter the administrative panel',
    'leave' => 'Leave',
    'my_account_settings' => 'My account settings',
    'new_user' => 'New user',
    'no_notification' => 'You have no notification',
    'notification_send_hour' => 'The user :name made an :type',
    'notification_title' => 'New notification',
    'registered_hour' => 'Registered hour',
    'registered_in' => 'Registered in',
    'settings' => 'Settings',
    'system_settings' => 'System Settings',
    'unlock_login_application' => 'Sign in to unlock the app',
    'user_timings' => 'User timings',
    'users' => 'Users',
    'welcome' => 'Welcome',

    'success' => [
        'settings_system_updated' => 'System settings successfully updated',
        'user_registered' => 'User successfully registered',
        'user_updated' => 'User successfully updated',
        'html' => [
            'create_or_view_all_users' => 'You can register a new user or return to <a href=":route">all Users</a>.',
            'continue_editing_or_view_all_users' => 'You can continue editing or return to <a href=":route">all Users</a>.'
        ],
    ],

    'replace' => [
        'singular_notifications' => 'You have :var notification',
        'plural_notifications' => 'You have :var notifications'
    ],

    'language' => [
        'pt-BR' => 'Portuguese Brazil',
        'en' => 'English'
    ],

    'role' => [
        'admin' => 'Administrator',
        'manager' => 'Manager',
        'simple' => 'Simple'
    ],

    'errors' => [
        '401' => 'You are not authorized to do this',
        '404' => 'Page not found'
    ],
];