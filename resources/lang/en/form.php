<?php

return [
    'field' => [
        'application_name' => 'Application name',
        'confirm_password' => 'Confirm password',
        'created_at' => 'Created at',
        "date_hour" => "Date and hour",
        'default_language' => 'Default Language',
        'email' => 'Email',
        'full_name' => 'Full name',
        'force_password_change' => 'Force password change',
        'image' => 'Image',
        'language' => 'Language',
        'logo' => 'Logo',
        'name' => 'Name',
        "observation" => "Observation",
        'observations' => 'Observations',
        'password' => 'Password',
        'role' => 'Role',
        'set_default_image' => 'Set default image',
        'show_notifications' => 'Show notifications',
        'type' => 'Type',
        'user' => 'User',
        'enable_hour_control' => 'Enable hour control',
        'enable_is_master' => 'User Master Level',
        'enable_is_master_explanation' => 'Only users with this option can unlock the application',
    ],

    'button' => [
        'administrative_panel' => 'Administrative panel',
        'close' => 'Close',
        'delete' => 'Delete',
        'edit' => 'Edit',
        'exit' => 'Exit',
        'enter' => 'Enter',
        'go_back' => 'Go back',
        'lock_application' => 'Lock Application',
        'register' => 'Register',
        'save' => 'Save',
        'settings' => 'Settings',
        'update' => 'Update'
    ],

    'placeholder' => [
        "confirm_password" => "Confirm the password",
        'enter_application_name' => 'Enter the name of your application',
        "enter_email" => "Enter email",
        "enter_full_name" => "Enter full name",
        "enter_new_password" => "Enter the new password",
        "enter_user_password" => "Enter the user's password",
        "enter_user_email" => "Enter user email",
        "enter_user_full_name" => "Enter the user's full name",
        "select_user" => "Select a user",
    ]
];