<?php

return [
    'dashboard' => 'Dashboard',
    'users' => 'Users',

    'settings' => [
        'title' => 'Settings',
        'me' => 'My Account',
        'system' => 'System'
    ]
];