<?php

return [
    'alert_force_password_change' => 'Você precisa alterar sua senha',
    'all_rights_reserved' => 'Todos os direitos reservados',
    'all_users' => 'Todos os usuários',
    'configure_your_system' => 'Configure seu sistema',
    'control_you_working_hours' => 'Controle suas horas de trabalho',
    'copyright' => 'Copyright',
    'create_user' => 'Cadastrar usuário',
    'edit_user' => 'Editar usuário',
    'entrance' => 'Entrada',
    'enter_administrative_panel' => 'Entrar no painel administrativo',
    'leave' => 'Saída',
    'my_account_settings' => 'Configurações da minha conta',
    'new_user' => 'Novo usuário',
    'no_notification' => 'Você não tem nenhuma notificação',
    'notification_send_hour' => 'O usuário :name fez uma :type',
    'notification_title' => 'Nova notificação',
    'registered_hour' => 'Hora registrada',
    'registered_in' => 'Registrando em',
    'settings' => 'Configurações',
    'system_settings' => 'Configurações do Sistema',
    'unlock_login_application' => 'Faça login para desbloquear a aplicação',
    'user_timings' => 'Horas dos usuários',
    'users' => 'Usuários',
    'welcome' => 'Seja bem-vindo',

    'success' => [
        'settings_system_updated' => 'Configurações do sistema atualizado com sucesso',
        'user_registered' => 'Usuário cadastrado com sucesso',
        'user_updated' => 'Usuário atualizado com sucesso',
        'html' => [
            'create_or_view_all_users' => 'Você pode cadastrar um novo usuário ou voltar para <a href=":route">todos os Usuários</a>.',
            'continue_editing_or_view_all_users' => 'Você pode continuar editando ou voltar para <a href=":route">todos os Usuários</a>.'
        ],
    ],

    'replace' => [
        'singular_notifications' => 'Você tem :var notificação',
        'plural_notifications' => 'Você tem :var notificações'
    ],

    'language' => [
        'pt-BR' => 'Português do Brasil',
        'en' => 'Inglês'
    ],

    'role' => [
        'admin' => 'Administrador',
        'manager' => 'Gerente',
        'simple' => 'Comum'
    ],

    'errors' => [
        '401' => 'Você não está autorizado a realizar isso',
        '404' => 'Página não encontrada'
    ],
];