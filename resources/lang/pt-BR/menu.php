<?php

return [
    'dashboard' => 'Início',
    'users' => 'Usuários',

    'settings' => [
        'title' => 'Configurações',
        'me' => 'Minha Conta',
        'system' => 'Sistema'
    ]
];