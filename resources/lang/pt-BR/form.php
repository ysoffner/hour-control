<?php

return [
    'field' => [
        'application_name' => 'Nome da Aplicação',
        'confirm_password' => 'Confirmar senha',
        'created_at' => 'Data de cadastro',
        "date_hour" => "Data e hora",
        'default_language' => 'Linguagem padrão',
        'email' => 'Email',
        'full_name' => 'Nome completo',
        'force_password_change' => 'Forçar alteração de senha',
        'image' => 'Imagem',
        'language' => 'Idioma',
        'logo' => 'Logo',
        'name' => 'Nome',
        "observation" => "Observação",
        'observations' => 'Observações',
        'password' => 'Senha',
        'role' => 'Cargo',
        'set_default_image' => 'Definir imagem padrão',
        'show_notifications' => 'Exibir notificações',
        'type' => 'Tipo',
        'user' => 'Usuário',
        'enable_hour_control' => 'Habilitar controle de horas',
        'enable_is_master' => 'Usuário nível Master',
        'enable_is_master_explanation' => 'Apenas usuários com essa opção poderão desbloquear a aplicação',
    ],

    'button' => [
        'administrative_panel' => 'Painel administrativo',
        'close' => 'Fechar',
        'delete' => 'Deletar',
        'edit' => 'Editar',
        'exit' => 'Sair',
        'enter' => 'Entrar',
        'go_back' => 'Voltar',
        'lock_application' => 'Bloquear aplicação',
        'register' => 'Cadastrar',
        'save' => 'Salvar',
        'settings' => 'Configurações',
        'update' => 'Atualizar'
    ],

    'placeholder' => [
        "confirm_password" => "Confirme a senha",
        'enter_application_name' => 'Digite o nome da sua aplicação',
        "enter_email" => "Digite a senha",
        "enter_full_name" => "Digite o nome completo",
        "enter_new_password" => "Digite a nova senha",
        "enter_user_password" => "Digite a senha do usuário",
        "enter_user_email" => "Digite o email do usuário",
        "enter_user_full_name" => "Digite o nome completo do usuário",
        "select_user" => "Selecione um usuário",
    ]
];