@extends("layouts.app")

@section('content-header')
    <h1>{{ trans('message.system_settings') }}</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ route('dashboard.settings.system') }}"
                  method="POST"
                  class="form-horizontal"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('message.configure_your_system') }}</h3>
                    </div>
                    <div class="box-body">
                        @if (session('_updated'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="alert alert-success">
                                        <strong>
                                            {{ trans('message.success.settings_system_updated') }}
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : null }}">
                            <label for="form-name" class="col-sm-2 control-label">{{ trans('form.field.application_name') }}</label>

                            <div class="col-sm-10">
                                <input type="text"
                                       id="form-name"
                                       name="name"
                                       class="form-control"
                                       placeholder="{{ trans("form.placeholder.enter_application_name") }}"
                                       value="{{ old('name') ? old('name') : $settings->get('name') }}"
                                >

                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('logo') ? 'has-error' : null }}">
                            <label for="form-logo" class="col-sm-2 control-label">{{ trans('form.field.logo') }}</label>

                            <div class="col-sm-10">
                                <input type="file"
                                       id="form-logo"
                                       name="logo"
                                >

                                @if ($errors->has('logo'))
                                    <span class="help-block">{{ $errors->first('logo') }}</span>
                                @endif

                                @if($settings->get('logo'))
                                    <div class="col-xs-4" style="padding-top: 10px;">
                                        <img src="{{ asset($settings->get('logo')) }}" class="img-responsive" alt="Logo Application">
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('default_language') ? 'has-error' : '' }}">
                            <label for="form-role" class="col-sm-2 control-label">{{ trans("form.field.default_language") }}</label>

                            <div class="col-sm-10">
                                <select id="form-default-language"
                                        name="default_language"
                                        class="form-control"
                                >
                                    <option value="en" {{ old('default_language') == 'en' || $settings->get('default_language') == 'en' ? 'selected' : '' }}>
                                        {{ trans('message.language.en') }}
                                    </option>
                                    <option value="pt-BR" {{ old('default_language') == 'pt-BR' || $settings->get('default_language') == 'pt-BR' ? 'selected' : '' }}>
                                        {{ trans('message.language.pt-BR') }}
                                    </option>
                                </select>

                                @if ($errors->has('default_language'))
                                    <span class="help-block">{{ $errors->first('default_language') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-sm btn-primary">
                            <i class="fa fa-check"></i>&nbsp;&nbsp;
                            {{ trans('form.button.save')  }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if (isset($user))
        <div id="modalDestroy" class="modal fade modal-danger" role="dialog">
            <form action="{{ route('dashboard.users.destroy', $user->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Você tem certeza?</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                Você tem certeza que deseja excluir o usuário <strong>{{ $user->name }}</strong>?
                                Esta ação não poderá ser desfeita
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger" data-dismill="modal">
                                {{ trans('form.button.delete') }}
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('form.button.close') }}</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    @endif
@endsection