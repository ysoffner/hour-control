@extends('layouts.app')

@section('content-header')
    <h1>{{ trans('message.my_account_settings') }}</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ route('dashboard.settings.me') }}" method="post" enctype="multipart/form-data">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ auth()->user()->name }} - {{ trans('message.settings') }}</h3>
                    </div>

                    <div class="box-body">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : null }}">
                                    <label for="form-name">{{ trans('form.field.full_name') }}</label>
                                    <input type="text"
                                           id="form-name"
                                           name="name"
                                           class="form-control"
                                           placeholder="{{ trans("form.placeholder.enter_full_name") }}"
                                           value="{{ auth()->user()->name }}"
                                    >

                                    @if ($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : null }}">
                                    <label for="form-email">{{ trans('form.field.email') }}</label>
                                    <input type="email"
                                           id="form-email"
                                           name="email"
                                           class="form-control"
                                           placeholder="{{ trans("form.placeholder.enter_email") }}"
                                           value="{{ auth()->user()->email }}"
                                    >

                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group {{ $errors->has('language') ? 'has-error' : null }}">
                                    <label for="form-language">{{ trans('form.field.language') }}</label>
                                    <select id="form-language"
                                            name="language"
                                            class="form-control"
                                    >
                                        <option value="en" {{ auth()->user()->language == 'en' ? 'selected' : '' }}>{{ trans('message.language.en') }}</option>
                                        <option value="pt-BR" {{ auth()->user()->language == 'pt-BR' ? 'selected' : '' }}>{{ trans('message.language.pt-BR') }}</option>
                                    </select>

                                    @if ($errors->has('language'))
                                        <span class="help-block">{{ $errors->first('language') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : null }}">
                                    <label for="form-password">{{ trans('form.field.password') }}</label>
                                    <input type="password"
                                           id="form-password"
                                           name="password"
                                           class="form-control"
                                           placeholder="{{ trans("form.placeholder.enter_new_password") }}"
                                    >

                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : null }}">
                                    <label for="form-password-confirmation">{{ trans('form.field.confirm_password') }}</label>
                                    <input type="password"
                                           id="form-password-confirmation"
                                           name="password_confirmation"
                                           class="form-control"
                                           placeholder="{{ trans("form.placeholder.confirm_password") }}"
                                    >

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-8">
                                <div class="form-group {{ $errors->has('image') ? 'has-error' : null }}">
                                    <label for="form-image">{{ trans('form.field.image') }}</label>
                                    <input type="file"
                                           id="form-image"
                                           name="image"
                                    >

                                    @if($errors->has('image'))
                                        <span class="help-block">{{ $errors->first('image') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4 text-center">
                                <img src="{{ asset(auth()->user()->image) }}" class="img-responsive">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> {{ trans('form.field.set_default_image') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-success btn-sm">
                            <i class="fa fa-check"></i>
                            {{ trans('form.button.save') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
