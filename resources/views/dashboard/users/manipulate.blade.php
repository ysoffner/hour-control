@extends("layouts.app")

@section('content-header')
    <h1>{{ isset($user) ? trans('message.edit_user') : trans('message.create_user') }}</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ isset($user) ? route('dashboard.users.update', $user->id) : route('dashboard.users.store') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                @if (isset($user))
                    {{ method_field('PUT') }}
                @endif
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ isset($user) ? $user->name : trans('message.new_user') }}</h3>
                        <div class="box-tools pull-right">
                            @if (isset($user))
                                <a href="#!" data-toggle="modal" data-target="#modalDestroy" class="btn btn-sm btn-danger">
                                    <i class="fa fa-times"></i>&nbsp;&nbsp;{{ trans('form.button.delete') }}
                                </a>
                            @endif
                            <a href="{{ route('dashboard.users.index') }}" class="btn btn-sm btn-success">
                                <i class="fa fa-reply"></i>&nbsp;&nbsp;{{ trans('form.button.go_back') }}
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        @if (session('_stored'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="alert alert-success">
                                        <strong>
                                            {{ trans('message.success.user_registered') }}
                                        </strong>
                                        <br>
                                        {!! trans('message.success.html.create_or_view_all_users', ['route' => route('dashboard.users.index')]) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (session('_updated'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="alert alert-success">
                                        <strong>
                                            {{ trans('message.success.user_updated') }}
                                        </strong>
                                        <br>
                                        {!! trans('message.success.html.continue_editing_or_view_all_users', ['route' => route('dashboard.users.index')]) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : null }}">
                            <label for="form-name" class="col-sm-2 control-label">{{ trans('form.field.full_name') }}</label>

                            <div class="col-sm-10">
                                <input type="text"
                                       id="form-name"
                                       name="name"
                                       class="form-control"
                                       placeholder="{{ trans("form.placeholder.enter_user_full_name") }}"
                                       value="{{ isset($user) ? $user->name : old('name') }}"
                                >

                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : null }}">
                            <label for="form-email" class="col-sm-2 control-label">{{ trans('form.field.email') }}</label>

                            <div class="col-sm-10">
                                <input type="email"
                                       id="form-email"
                                       name="email"
                                       class="form-control"
                                       placeholder="{{ trans("form.placeholder.enter_user_email") }}"
                                       value="{{ isset($user) ? $user->email : old('email') }}"
                                >

                                @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : null }}">
                            <label for="form-password" class="col-sm-2 control-label">{{ trans('form.field.password') }}</label>

                            <div class="col-sm-10">
                                <input type="password"
                                       id="form-password"
                                       name="password"
                                       class="form-control"
                                       placeholder="{{ trans("form.placeholder.enter_new_password") }}"
                                >

                                @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : null }}">
                            <label for="form-password-confirmation" class="col-sm-2 control-label">{{ trans('form.field.confirm_password') }}</label>

                            <div class="col-sm-10">
                                <input type="password"
                                       id="form-password-confirmation"
                                       name="password_confirmation"
                                       class="form-control"
                                       placeholder="{{ trans("form.placeholder.confirm_password") }}"
                                >

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('force_password_change') ? 'has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-2">
                                <div class="checkbox no-padding">
                                    <label>
                                        <input type="checkbox"
                                               id="form-force-password-change"
                                               name="force_password_change"
                                               value="1"
                                               @if ((isset($user) && $user->force_password_change) || old('force_password_change'))
                                                   checked
                                               @endif
                                        > {{ trans('form.field.force_password_change') }}
                                    </label>
                                </div>

                                @if ($errors->has('force_password_change'))
                                    <span class="help-block">{{ $errors->first('force_password_change') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                            <label for="form-role" class="col-sm-2 control-label">{{ trans("form.field.role") }}</label>

                            <div class="col-sm-10">
                                <select id="form-role"
                                        name="role"
                                        class="form-control"
                                >
                                    <option value="admin" {{ (isset($user) && $user->role == 'admin') || old('role') == 'admin' ? 'selected' : '' }}>
                                        {{ trans('message.role.admin') }}
                                    </option>
                                    <option value="manager" {{ (isset($user) && $user->role == 'manager') || old('role') == 'manager' ? 'selected' : '' }}>
                                        {{ trans('message.role.manager') }}
                                    </option>
                                    <option value="simple" {{ (isset($user) && $user->role == 'simple') || old('role') == 'simple' ? 'selected' : '' }}>
                                        {{ trans('message.role.simple') }}
                                    </option>
                                </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">{{ $errors->first('role') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('hour_control') ? 'has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-2">
                                <div class="checkbox no-padding">
                                    <label>
                                        <input type="checkbox"
                                               id="form-hour-control"
                                               name="hour_control"
                                               value="1"
                                               @if ((isset($user) && $user->hour_control) || old('hour_control'))
                                                   checked
                                               @endif
                                        > {{ trans('form.field.enable_hour_control') }}
                                    </label>
                                </div>

                                @if ($errors->has('hour_control'))
                                    <span class="help-block">{{ $errors->first('hour_control') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                            <label for="form-role" class="col-sm-2 control-label">{{ trans("form.field.language") }}</label>

                            <div class="col-sm-10">
                                <select id="form-language"
                                        name="language"
                                        class="form-control"
                                >
                                    <option value="en" {{ (isset($user) && $user->language == 'en') || old('language') == 'en' ? 'selected' : '' }}>
                                        {{ trans('message.language.en') }}
                                    </option>
                                    <option value="pt-BR" {{ (isset($user) && $user->language == 'pt-BR') || old('language') == 'pt-BR' ? 'selected' : '' }}>
                                        {{ trans('message.language.pt-BR') }}
                                    </option>
                                </select>

                                @if ($errors->has('language'))
                                    <span class="help-block">{{ $errors->first('language') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('is_master') ? 'has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-2">
                                <div class="checkbox no-padding">
                                    <label>
                                        <input type="checkbox"
                                               id="form-is-master"
                                               name="is_master"
                                               value="1"
                                               @if ((isset($user) && $user->is_master) || old('is_master'))
                                               checked
                                                @endif
                                        >
                                        {{ trans('form.field.enable_is_master') }}

                                        <span class="help-block">{{ trans('form.field.enable_is_master_explanation') }}</span>
                                    </label>
                                </div>

                                @if ($errors->has('is_master'))
                                    <span class="help-block">{{ $errors->first('is_master') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-sm btn-primary">
                            <i class="fa fa-check"></i>&nbsp;&nbsp;
                            {{ isset($user) ? trans('form.button.update') : trans('form.button.save') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if (isset($user))
        <div id="modalDestroy" class="modal fade modal-danger" role="dialog">
            <form action="{{ route('dashboard.users.destroy', $user->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Você tem certeza?</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                Você tem certeza que deseja excluir o usuário <strong>{{ $user->name }}</strong>?
                                Esta ação não poderá ser desfeita
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger" data-dismill="modal">
                                {{ trans('form.button.delete') }}
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('form.button.close') }}</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    @endif
@endsection