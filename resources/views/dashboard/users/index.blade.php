@extends('layouts.app')

@section('content-header')
    <h1>{{ trans('message.users') }}</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ trans('message.all_users') }}</h3>
                    <div class="pull-right box-tools">
                        <a href="{{ route('dashboard.users.create') }}" class="btn btn-sm btn-success">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;{{ trans('form.button.register') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-condensed">
                        <tr>
                            <th>#</th>
                            <th>{{ trans('form.field.name') }}</th>
                            <th>{{ trans('form.field.email') }}</th>
                            <th>{{ trans('form.field.created_at') }}</th>
                            <th>#</th>
                        </tr>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('dashboard.users.edit', $user->id) }}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-edit"></i> {{ trans('form.button.edit') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="box-footer clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <div style="margin-top: 6px">
                                {{ trans('pagination.showing') }}
                                <strong>{{ $users->firstItem() }}</strong>
                                {{ strtolower(trans('pagination.until')) }}
                                <strong>{{ $users->lastItem() }}</strong>
                                {{ strtolower(trans('pagination.in')) }}
                                <strong>{{ $users->total() }}</strong>.
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="pull-right" style="margin-top: -22px; margin-bottom: -22px;">{{ $users->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
