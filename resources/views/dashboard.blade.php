@extends('layouts.app')

@section('content')
<div class="container">
    @can('monitor-control', \App\UsersHour::class)
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('message.user_timings') }}
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>
                                    {{ trans("form.field.name") }}
                                </th>
                                <th>
                                    {{ trans("form.field.type") }}
                                </th>
                                <th>
                                    {{ trans("form.field.date_hour") }}
                                </th>
                                <th>
                                    {{ trans("form.field.observation") }}
                                </th>
                            </tr>
                            @foreach ($users as $user)
                                <tr is="user-hour-table-item"
                                    name="{{ $user->name }}"
                                    :type="{{ $user->type }}"
                                    day="{{ $user->day->format('d/m/Y') != \Carbon\Carbon::now()->format('d/m/Y') ? $user->day->format('d/m/Y H:i') : $user->day->format('H:i') }}"
                                    :id="{{ $user->id }}"
                                    observation="{{ $user->observation }}"
                                    :trans-type="{1: '{{ trans('message.entrance') }}', 2: '{{ trans('message.leave') }}'}"
                                ></tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    @cannot('monitor-control', \App\UsersHour::class)
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <h4>{{ trans('message.welcome') }}.</h4>
                    </div>
                </div>
            </div>
        </div>
    @endcannot
</div>
@endsection
