<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ system_setting('name', 'app.name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset("css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/">
            <i class="fa fa-unlock"></i>
        </a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">
            {{ trans('message.unlock_login_application') }}
        </p>

        <form action="{{ route('unlock') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email"
                       id="form-email"
                       name="email"
                       placeholder="{{ trans('form.field.email') }}"
                       class="form-control"
                       value="{{ old('email') }}"
                       autofocus
                       required
                >
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                @if ($errors->has('email'))
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password"
                       id="form-password"
                       name="password"
                       placeholder="{{ trans('form.field.password') }}"
                       class="form-control"
                       required
                >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password'))
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        {{ trans('form.button.enter') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset ("js/AdminLTE.min.js") }}" type="text/javascript"></script>
</body>
</html>
