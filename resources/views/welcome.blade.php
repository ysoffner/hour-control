<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ system_setting('name', 'app.name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset("css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />

    <style>
        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="top-right links">
    <a href="{{ route('lock') }}">
        <i class="fa fa-lock"></i> {{ trans('form.button.lock_application') }}
    </a>
    <a href="{{ route('auth.login') }}">
        {{ trans('form.button.administrative_panel') }}
    </a>
</div>
<div class="login-box" id="app">
    <div class="login-logo">
        <a href="/">
            @if (! system_setting('logo') && system_setting('name', 'app.name'))
                <b>{{ system_setting('name', 'app.name') }}</b>
            @endif

            @if (system_setting('logo'))
                <img src="{{ asset(system_setting('logo')) }}"
                     alt="{{ system_setting('name', 'app.name') }}"
                     title="{{ system_setting('name', 'app.name') }}"
                     class="img-responsive"
                     style="max-height: 50px; margin: 0 auto 0 auto;">
            @endif
        </a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">
            {{ trans('message.control_you_working_hours') }}
        </p>

        <form action="{{ route('welcome') }}" method="post">
            {{ csrf_field() }}

            @if (session('_sent'))
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-success text-center">
                            <strong> {{ trans('message.registered_hour') }}!</strong>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group text-left {{ $errors->has('user') ? 'has-error' : '' }}">
                        <label for="form-user">{{ trans('form.field.user') }}</label>
                        <select name="user" id="form-user" class="form-control">
                            <option value="0" disabled selected>
                                {{ trans("form.placeholder.select_user") }}
                            </option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ old('user') == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('user'))
                            <span class="help-block">{{ $errors->first('user') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group text-left {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="form-password">{{ trans('form.field.password') }}</label>
                        <input type="password"
                               id="form-password"
                               name="password"
                               class="form-control"
                               placeholder="{{ trans('form.placeholder.enter_user_password') }}"
                        >

                        @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group text-left {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label for="form-type">{{ trans('form.field.type') }}</label>
                        <select name="type" id="form-type" class="form-control">
                            <option value="1" {{ old('type') == 1 ? 'checked' : '' }}>Entrada</option>
                            <option value="2" {{ old('type') == 2 ? 'checked' : '' }}>Saída</option>
                        </select>

                        @if ($errors->has('type'))
                            <span class="help-block">{{ $errors->first('type') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group text-left {{ $errors->has('observations') ? 'has-error' : '' }}">
                        <label for="form-observations">{{ trans('form.field.observations') }}</label>
                        <textarea name="observations" id="form-observations" class="form-control" rows="2"></textarea>

                        @if ($errors->has('observations'))
                            <span class="help-block">{{ $errors->first('observations') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        {{ trans('form.button.enter') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset ("js/AdminLTE.min.js") }}" type="text/javascript"></script>
</body>
</html>
