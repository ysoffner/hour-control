<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ system_setting('name', 'app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset("css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("css/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
</head>
<body class="skin-blue">
    <div class="wrapper" id="app">
        @include('layouts.app.header')
        @include('layouts.app.sidebar')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield("content-header")
            </section>

            <section class="content">
                @yield('content')
            </section>
        </div>

        @include('layouts.app.footer')
    </div>

    <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
    <script>
        window.firebaseApp = firebase.initializeApp({
            apiKey: "{{ env('FIREBASE_APIKEY') }}",
            authDomain: "{{ env('FIREBASE_AUTHDOMAIN') }}",
            databaseURL: "{{ env('FIREBASE_DATABASEURL') }}",
            projectId: "{{ env('FIREBASE_PROJECTID') }}",
            storageBucket: "{{ env('FIREBASE_STORAGEBUCKET') }}",
            messagingSenderId: "{{ env('FIREBASE_MESSAGINGSENDERID') }}"
        });
        @if (auth()->check())
            window.authUser = {
                id: '{{ auth()->user()->id }}',
                name: '{{ auth()->user()->name }}',
                email: '{{ auth()->user()->email }}',
                language: '{{ auth()->user()->language }}',
            };
        @endif
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset ("js/AdminLTE.min.js") }}" type="text/javascript"></script>
</body>
</html>
