<header class="main-header">
    <a href="{{ route('dashboard') }}" class="logo">
        @if (! system_setting('logo') && system_setting('name', 'app.name'))
            <b>{{ system_setting('name', 'app.name') }}</b>
        @endif

        @if (system_setting('logo'))
            <img src="{{ asset(system_setting('logo')) }}"
                 alt="{{ system_setting('name', 'app.name') }}"
                 title="{{ system_setting('name', 'app.name') }}"
                 class="img-responsive"
                 style="max-height: 90%; margin: 0 auto 0 auto; padding-top: 2.5%;">
        @endif
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                @can('receive-notifications', \App\User::class)
                    <notifications trans-show-notifications="{{ trans('form.field.show_notifications') }}"
                                   trans-no-notification="{{ trans('message.no_notification') }}"
                                   trans-singular-notifications="{{ trans('message.replace.singular_notifications', ['var' => 'number']) }}"
                                   trans-plural-notifications="{{ trans('message.replace.plural_notifications', ['var' => 'number']) }}"
                                   trans-notification-title="{{ trans('message.notification_title') }}"
                    ></notifications>
                @endcan

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset(auth()->user()->image) }}" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{ auth()->user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset(auth()->user()->image) }}" class="img-circle" alt="User Image" />
                            <p>
                                {{ auth()->user()->name }} - {{ auth()->user()->role_name }}
                                <small>{{ trans('message.registered_in') }} {{ auth()->user()->created_at->format('d/m/Y H:i') }}</small>
                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('dashboard.settings.me') }}" class="btn btn-default btn-flat">
                                    {{ trans('form.button.settings') }}
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('auth.logout') }}" class="btn btn-default btn-flat">
                                    {{ trans('form.button.exit') }}
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>