<footer class="main-footer">
    <div class="pull-right hidden-xs">
        {{ system_setting('name', 'app.name') }}
    </div>
    <strong>{{ trans('message.copyright') }} © 2017 <a href="https://github.com/omarkdev" target="_blank">omarkdev</a>.</strong> {{ trans('message.all_rights_reserved') }}.
</footer>