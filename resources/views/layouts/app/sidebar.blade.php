<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(auth()->user()->image)  }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li class="{{ menu_active('dashboard') }}">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> {{ trans('menu.dashboard') }}
                </a>
            </li>
            @can('view', \App\User::class)
                <li class="{{ menu_active('dashboard/users', true) }}">
                    <a href="{{ route('dashboard.users.index') }}">
                        <i class="fa fa-users"></i> {{ trans('menu.users') }}
                    </a>
                </li>
            @endcan

            @can('view', \App\Setting::class)
                <li class="treeview {{ menu_active('dashboard/settings', true) }}">
                    <a href="#">
                        <i class="fa fa-cogs"></i> <span>{{ trans('menu.settings.title') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ menu_active('dashboard/settings/me') }}">
                            <a href="{{ route('dashboard.settings.me') }}">
                                <i class="fa fa-circle-o"></i> {{ trans('menu.settings.me') }}
                            </a>
                        </li>

                        <li class="{{ menu_active('dashboard/settings/system') }}">
                            <a href="{{ route('dashboard.settings.system') }}">
                                <i class="fa fa-circle-o"></i> {{ trans('menu.settings.system') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            @cannot('view', \App\Setting::class)
                <li class="{{ menu_active('dashboard/settings/me') }}">
                    <a href="{{ route('dashboard.settings.me') }}">
                        <i class="fa fa-cogs"></i> {{ trans('menu.settings.title') }}
                    </a>
                </li>
            @endcannot
        </ul>
    </section>
</aside>