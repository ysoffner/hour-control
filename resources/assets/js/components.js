import Vue from 'vue'

var components = [
    'user-hour-table-item',
    'notifications'
];

components.map((item) => Vue.component(item, require(`./components/${item}.vue`)));