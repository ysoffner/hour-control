<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/unlock', 'UnlockController@showLoginForm')->name('unlock');
Route::post('/unlock', 'UnlockController@login');

Route::middleware('auth.unlock')
    ->group(function() {
        Route::get('/lock', 'UnlockController@logout')->name('lock');

        Route::get('/', 'WelcomeController@index')->name('welcome');
        Route::post('/', 'WelcomeController@sendHour');
    });

Route::name('auth.')
    ->prefix('auth')
    ->namespace('Auth')
    ->group(function() {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');

        Route::get('logout', 'LoginController@logout')->name('logout')->middleware('auth');
    });

Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');
Route::name('dashboard.')
    ->prefix('dashboard')
    ->namespace('Dashboard')
    ->middleware('auth')
    ->group(function() {
        Route::get('/force_password_change', 'ForcePasswordChangeController@index')->name('force_password_change');
        Route::post('/force_password_change', 'ForcePasswordChangeController@update');

        Route::name('settings.')
            ->prefix('settings')
            ->namespace('Settings')
            ->group(function() {
                Route::get('/me', 'MeController@index')->name('me');
                Route::put('/me', 'MeController@update');

                Route::get('/system', 'SystemController@index')->name('system');
                Route::post('/system', 'SystemController@update');
            });

        Route::resource('users', 'UserController', ['except' => [
            'show'
        ]]);
    });
