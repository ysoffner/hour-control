<?php

namespace App\Factories;

use Firebase\FirebaseLib;

class FirebaseFactory
{
    /**
     * @var FirebaseLib
     */
    protected $firebase;

    /**
     * FireBase Default Path.
     *
     * @var string
     */
    protected $defaultPath = 'hour-control/';

    /**
     * FirebaseFactory constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $token = env('FIREBASE_TOKEN');
        $databaseUrl = env('FIREBASE_DATABASEURL');

        if (! $token || ! $databaseUrl) {
            throw new \Exception("Firebase not configured.");
        }

        $this->firebase = new FirebaseLib($databaseUrl, $token);
    }

    /**
     * Store data in FireBase.
     *
     * @param $path
     * @param $value
     * @return array
     */
    public function set($path, $value)
    {
        $path = $this->defaultPath . $path;

        return $this->firebase->set($path, $value);
    }

    /**
     * Reads a value from FireBase.
     *
     * @param $path
     * @return array
     */
    public function get($path)
    {
        $path = $this->defaultPath . $path;

        return $this->firebase->get($path);
    }

    /**
     * Deletes value from FireBase.
     *
     * @param $path
     * @return array
     */
    public function delete($path)
    {
        $path = $this->defaultPath . $path;

        return $this->firebase->delete($path);
    }

    /**
     * Updates data in FireBase.
     *
     * @param $path
     * @param array $data
     * @return array
     */
    public function update($path, Array $data)
    {
        $path = $this->defaultPath . $path;

        return $this->firebase->update($path, $data);
    }

    /**
     * Push data to FireBase.
     *
     * @param $path
     * @param array $data
     * @return array
     */
    public function push($path, Array $data)
    {
        $path = $this->defaultPath . $path;

        return $this->firebase->push($path, $data);
    }

    /**
     * Send the method to the FirebaseLib.
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->firebase->$name(... $arguments);
    }
}