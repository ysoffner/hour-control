<?php

namespace App;

use App\Traits\Models\ImageTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use ImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image', 'force_password_change', 'role', 'hour_control', 'language', 'is_master'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'force_password_change' => 'boolean',
        'hour_control' => 'boolean',
        'is_master' => 'boolean'
    ];

    /**
     * Default name for images being uploaded
     *
     * @var string
     */
    protected $defaultImage = 'default-user.png';

    /**
     * Default path to upload images
     *
     * @var string
     */
    protected $pathUpload = '/upload/avatars/';

    /**
     * Sets encryption in password.
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * The user's role name.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getRoleNameAttribute()
    {
        $role = $this->getAttribute('role');

        return trans("message.role.{$role}");
    }

    /**
     * User hours.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hours()
    {
        return $this->hasMany('App\UsersHour', 'user_id', 'id');
    }
}
