<?php

namespace App\Repositories;

use App\UsersHour;
use Illuminate\Support\Facades\App;

class UsersHourRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UsersHour::class;
    }

    /**
     * The last "type" of the user.
     *
     * @param $user
     * @return mixed|null
     */
    public function getLastTypeUser($user)
    {
        $lastHour = $this->model
            ->select('type')
            ->where('user_id', $user)
            ->orderBy('created_at', 'DESC')
            ->first();

        return ! $lastHour ? null : $lastHour->type;
    }

    /**
     * Save a new model in the database..
     *
     * @param array $data
     * @return mixed
     */
    public function create(Array $data)
    {
        App::make('App\Factories\FirebaseFactory')
            ->push('users/hours/user_' . $data['user_id'], [
                'type' => $data['type'],
                'observation' => $data['observation'],
                'day' => $data['day']->format('H:i')
            ]);

        return parent::create($data);
    }
}