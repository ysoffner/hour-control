<?php

namespace App\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseRepository
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Repository constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    abstract function model();

    /**
     * The Model to be used in the repository.
     *
     * @return Model|mixed
     * @throws \Exception
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (! $model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Clear empty values.
     *
     * @param array $data
     * @return array
     */
    public function clearEmptyValues(Array $data)
    {
        return collect($data)
            ->filter(function ($value, $key) {
                return (is_bool($value) || is_numeric($value) || ! empty($value));
            })
            ->toArray();
    }

    /**
     * Paginate the model.
     *
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * Save a new model in the database.
     *
     * @param array $data
     * @return mixed
     */
    public function create(Array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update the model in the database.
     *
     * @param array $data
     * @param $value
     * @param string $field
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function update(Array $data, $value, $field = "id")
    {
        $model = $this->findBy($field, $value);
        $model->update($this->clearEmptyValues($data));

        return $model;
    }

    /**
     * Delete the model from the database.
     *
     * @param $id
     * @param array $columnsFind
     * @return mixed
     */
    public function delete($id, $columnsFind = ['id'])
    {
        return $this->find($id, $columnsFind)->delete();
    }

    /**
     * Find a model by its primary key.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id, $columns = ['*'])
    {
        $model = $this->model->find($id, $columns);

        if (! $model) {
            throw (new ModelNotFoundException())->setModel($this->model);
        }

        return $model;
    }

    /**
     * Find a model by a custom field.
     *
     * @param $field
     * @param $value
     * @param array $columns
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findBy($field, $value, $columns = ['*'])
    {
        $model = $this->model->where($field, "=", $value)
            ->first($columns);

        if (! $model) {
            throw (new ModelNotFoundException())->setModel($this->model);
        }

        return $model;
    }

    /**
     * Send the method to the model.
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->model->$name(... $arguments);
    }
}