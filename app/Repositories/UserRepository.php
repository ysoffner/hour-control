<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Paginate the model results.
     *
     * @param int $perPage
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->paginate($perPage, $columns);
    }

    /**
     * Update the user in the database.
     *
     * @param array $data
     * @param $value
     * @param string $field
     * @return mixed
     */
    public function update(Array $data, $value, $field = "id")
    {
        $model = $this->findBy($field, $value);

        if (isset($data['image']) && $data['image']) {
            $model->deleteImage();
        }

        $model->update($this->clearEmptyValues($data));

        return $model;
    }

    /**
     * Delete the model from the database.
     *
     * @param $id
     * @param array $columnsFind
     * @return mixed
     */
    public function delete($id, $columnsFind = ['id', 'image'])
    {
        $user = $this->find($id, $columnsFind);

        $user->deleteImage()
            ->delete();

        return $user;
    }

    /**
     * Users with their last hours.
     */
    public function lastHours()
    {
        return $this->model
            ->select('id', 'name')
            ->get()
            ->map(function($item) {
                $hour = $item->hours()
                    ->select('day', 'type', 'observation')
                    ->orderBy('created_at', 'DESC')
                    ->first();

                if (! $hour) {
                    return $item;
                }

                $item->day = $hour->day;
                $item->type = $hour->type;
                $item->observation = $hour->observation;

                return $item;
            })
            ->filter
            ->day;
    }
}