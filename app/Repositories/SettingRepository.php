<?php

namespace App\Repositories;

use App\Setting;

class SettingRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Setting::class;
    }

    /**
     * All settings defined by the key.
     *
     * @return \Illuminate\Support\Collection|static
     */
    public function allByKeyValue()
    {
        return $this->model
            ->select('key', 'value', 'type')
            ->get()
            ->keyBy('key')
            ->map(function($item) {
                return $item->value;
            });
    }

    /**
     * Synchronize settings based on the keys.
     *
     * @param array $settings
     * @return $this
     */
    public function syncKeys(Array $settings)
    {
        $settings = $this->clearEmptyValues($settings);

        foreach ($settings as $key => $value) {
            $setting = $this->model->firstOrNew(compact('key'));
            $setting->value = $value;

            $setting->save();
        }

        return $this;
    }
}