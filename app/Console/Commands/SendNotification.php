<?php

namespace App\Console\Commands;

use App\Factories\FirebaseFactory;
use App\Services\NotificationService;
use Illuminate\Console\Command;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:send {trans*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to all logged in users';

    /**
     * @var NotificationService
     */
    protected $notificationService;

    /**
     * Create a new command instance.
     *
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $trans = $this->argument('trans');

        $this->notificationService->send(join(' ', $trans));

        return $this->info('Notification sent!');
    }
}
