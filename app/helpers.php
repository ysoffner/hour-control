<?php

if (! function_exists("menu_active")) {
    /**
     * Verify that the route is active
     * and return the class name.
     *
     * @param $route
     * @param bool $acceptWildcard
     * @return string
     */
    function menu_active($route, $acceptWildcard = false)
    {
        if (! $acceptWildcard) {
            return request()->is($route) ? 'active' : '';
        }

        return request()->is($route) || request()->is("{$route}/*") ? 'active' : '';
    }
}

if (! function_exists('system_setting')) {
    /**
     * Obtain system configuration or an application config.
     *
     * @param $key
     * @param null $defaultConfigApp
     * @return mixed|null
     */
    function system_setting($key, $defaultConfigApp = null)
    {
        static $settings = [];

        if (isset($settings[$key])) {
            return $settings[$key];
        }

        $setting = \App\Setting::select('value', 'type')
            ->where('key', $key)
            ->first();

        if (! $setting || ! $setting->value) {
            $value = $defaultConfigApp ? config($defaultConfigApp) : null;

            return $settings[$key] = $value;
        }

        return $settings[$key] = $setting->value;
    }
}