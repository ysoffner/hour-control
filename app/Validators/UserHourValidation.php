<?php

namespace App\Validators;

use App\Repositories\UsersHourRepository;

class UserHourValidation
{
    /**
     * @var UsersHourRepository
     */
    protected $usersHours;

    /**
     * UserHourValidation constructor.
     *
     * @param UsersHourRepository $usersHours
     */
    public function __construct(UsersHourRepository $usersHours)
    {
        $this->usersHours = $usersHours;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $fields = $validator->getData();

        if (! isset($fields['user']) || ! isset($fields['type'])) {
            return false;
        }

        $lastHour = $this->usersHours->getLastTypeUser($fields['user']);

        return (int) $lastHour === (int) $fields['type'] ? false : true;
    }
}