<?php

namespace App\Http\Requests\Dashboard\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|max:255|min:6',
            'email' => [
                'nullable',
                'max:255',
                Rule::unique('users')->ignore($this->user)
            ],
            'password' => 'nullable|min:6|confirmed',
            'force_password_change' => 'nullable|boolean',
            'role' => 'nullable|in:admin,manager,simple',
            'hour_control' => 'nullable|boolean',
            'language' => 'nullable|in:en,pt-BR',
            'is_master' => 'nullable|boolean',
        ];
    }
}
