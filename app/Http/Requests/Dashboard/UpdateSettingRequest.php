<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|max:255|min:6',
            'email' => [
                'nullable',
                'max:255',
                Rule::unique('users')->ignore(auth()->user()->id)
            ],
            'password' => 'nullable|min:6|confirmed',
            'image' => 'nullable|image',
            'language' => 'nullable|in:en,pt-BR'
        ];
    }
}
