<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendHourWelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'required|exists:users,id',
            'password' => 'required|min:6',
            'type' => 'required|in:1,2|user_hour',
        ];
    }
}
