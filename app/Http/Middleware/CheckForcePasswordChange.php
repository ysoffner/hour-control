<?php

namespace App\Http\Middleware;

use Closure;

class CheckForcePasswordChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->checkRedirectToForcePasswordChange($request)) {
            return redirect()->route('dashboard.force_password_change');
        }

        return $next($request);
    }

    /**
     * Check if it is to redirect to the
     * force password change page
     *
     * @param $request
     * @return bool
     */
    protected function checkRedirectToForcePasswordChange($request)
    {
        if (! auth()->check() || ! auth()->user()->force_password_change) {
            return false;
        }

        if ($request->route()->getName() == "dashboard.force_password_change") {
            return false;
        }

        return $request->url() == route('dashboard.force_password_change') ? false : true;
    }
}
