<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthUnlock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->guard()->check()) {
            $user = $this->guard()->user();

            if (! $user->is_master) {
                $this->logoutUserAuthenticated();
                return $this->redirectToUnlock();
            }

            return $next($request);
        }

        return $this->redirectToUnlock();
    }

    /**
     * Disconnect authenticated user.
     *
     * @return mixed
     */
    protected function logoutUserAuthenticated()
    {
        return $this->guard()->logout();
    }

    /**
     * Redirect to the unlock application page.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectToUnlock()
    {
        return redirect()->route('unlock');
    }

    /**
     * Get the guard to be used during manipulation.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('unlock');
    }
}
