<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = system_setting('default_language', 'app.locale');

        if (Auth::check() || $request->get('language')) {
            $language = Auth::check() ? Auth::user()->language : $request->get('language');
        }

        $this->changeLanguage($language);

        return $next($request);
    }

    /**
     * Change language to user
     * language.
     *
     * @param $language
     * @return mixed
     */
    protected function changeLanguage($language)
    {
        return App::setLocale($language);
    }
}
