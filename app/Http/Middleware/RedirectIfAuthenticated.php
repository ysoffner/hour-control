<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @param string $route
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $route = "/dashboard")
    {
        if (Auth::guard($guard)->check()) {
            return redirect($route);
        }

        return $next($request);
    }
}
