<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * DashboardController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show the application welcome page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->users->lastHours();

        return view('dashboard', compact('users'));
    }
}
