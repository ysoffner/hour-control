<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\TranslateRequest;
use App\Http\Controllers\Controller;

class TranslateController extends Controller
{
    /**
     * Translate texts.
     *
     * @param TranslateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function translate(TranslateRequest $request)
    {
        return response()->json([
            'message' => trans($request->get('id'), $request->get('arguments'))
        ], 200);
    }
}
