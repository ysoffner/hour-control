<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendHourWelcomeRequest;
use App\Repositories\UserRepository;
use App\Repositories\UsersHourRepository;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var UsersHourRepository
     */
    protected $usersHours;

    /**
     * WelcomeController constructo
     * r.
     * @param UserRepository $users
     * @param UsersHourRepository $usersHour
     */
    public function __construct(UserRepository $users, UsersHourRepository $usersHour)
    {
        $this->users = $users;
        $this->usersHours = $usersHour;
    }

    /**
     * Show the application welcome page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->users
            ->select('id', 'name')
            ->where('hour_control', true)
            ->get();

        return view('welcome', compact('users'));
    }

    /**
     * Send user time.
     *
     * @param SendHourWelcomeRequest $request
     * @param NotificationService $notificationService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendHour(SendHourWelcomeRequest $request, NotificationService $notificationService)
    {
        $user = $request->get('user');
        $password = $request->get('password');
        $type = $request->get('type');

        if (! Auth::attempt(['id' => $user, 'password' => $password])) {
            return redirect()->back()
                ->withInput()
                ->withErrors([
                    'password' => 'Senha inválida'
                ]);
        }

        $this->usersHours->create([
            'day' => Carbon::now(),
            'type' => $type,
            'user_id' => $user,
            'observation' => $request->get('observations')
        ]);

        $notificationParams = $this->getNotificationParams($user, $type);
        $notificationService->send($notificationParams['trans'], $notificationParams['arguments']);

        return back()->with('_sent', true);
    }

    /**
     * The notification message.
     *
     * @param $user
     * @param $type
     * @return array
     */
    protected function getNotificationParams($user, $type)
    {
        $typeName = $type == 1 ? trans('message.entrance') : trans('message.leave');
        $userName = $this->users
            ->select('name')
            ->where('id', $user)
            ->first()
            ->name;

        return [
            'trans' => 'message.notification_send_hour',
            'arguments' => [
                'name' => $userName,
                'type' => $typeName
            ]
        ];
    }
}
