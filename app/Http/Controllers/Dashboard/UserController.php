<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\Users\StoreUserRequest;
use App\Http\Requests\Dashboard\Users\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * UserController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('view', User::class);
        $users = $this->users->paginate(15);

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', User::class);

        return view('dashboard.users.manipulate');
    }

    /**
     * Store a newly created user in storage.
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('create', User::class);

        $fields = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'force_password_change' => $request->has('force_password_change'),
            'role' => $request->get('role'),
            'hour_control' => $request->has('hour_control'),
            'language' => $request->get('language'),
            'is_master' => $request->has('is_master')
        ];
        $this->users->create($fields);

        return back()->with('_stored', true);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($user)
    {
        $this->authorize('update', User::class, $user);

        $user = $this->users->find($user);

        return view('dashboard.users.manipulate', compact('user'));
    }

    /**
     * Update the specified user in storage.
     *
     * @param UpdateUserRequest $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $user)
    {
        $this->authorize('update', User::class, $user);

        $fields = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'force_password_change' => $request->has('force_password_change'),
            'role' => $request->get('role'),
            'hour_control' => $request->has('hour_control'),
            'language' => $request->get('language'),
            'is_master' => $request->has('is_master')
        ];
        $this->users->update($fields, $user);

        return back()->with('_updated', true);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return string
     */
    public function destroy($user)
    {
        $this->authorize('delete', User::class, $user);

        $this->users->delete($user);

        return redirect()->route('dashboard.users.index');
    }
}
