<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\UpdateForcePasswordChangeRequest;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class ForcePasswordChangeController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * ForcePasswordChangeController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Form to force password change.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (! auth()->user()->force_password_change) {
            $route = request()->url() == redirect()->back()->getTargetUrl() ? route('dashboard') : redirect()->back()->getTargetUrl();

            return redirect()->route('dashboard');
        }

        return view('dashboard.force_password_change');
    }

    /**
     * Update the password of the authenticated user.
     *
     * @param UpdateForcePasswordChangeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateForcePasswordChangeRequest $request)
    {
        $this->users->update([
            'password' => $request->get('password'),
            'force_password_change' => false
        ], auth()->user()->id);

        return back();
    }
}
