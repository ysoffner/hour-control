<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Requests\Dashboard\Settings\UpdateSystemRequest;
use App\Repositories\SettingRepository;
use App\Http\Controllers\Controller;
use App\Setting;

class SystemController extends Controller
{
    /**
     * @var SettingRepository
     */
    protected $settings;

    /**
     * SystemController constructor.
     *
     * @param SettingRepository $settings
     */
    public function __construct(SettingRepository $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('view', Setting::class);
        
        $settings = $this->settings->allByKeyValue();

        return view('dashboard.settings.system', compact('settings'));
    }

    /**
     * @param UpdateSystemRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSystemRequest $request)
    {
        $this->authorize('update', Setting::class);
        
        $fields = $request->only('name', 'logo', 'default_language');
        $this->settings->syncKeys($fields);

        return back()->with('_updated', true);
    }
}
