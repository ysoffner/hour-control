<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Requests\Dashboard\UpdateSettingRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class MeController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * MeController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Display a settings page from
     * authenticated user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.settings.me');
    }

    /**
     * Update the authenticated user
     * resource in storage.
     *
     * @param UpdateSettingRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSettingRequest $request)
    {
        $user = $this->users->update(
            $request->only('name', 'email', 'password', 'image', 'language'),
            auth()->user()->id
        );

        return redirect()->back();
    }
}
