<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'value'
    ];

    /**
     * Default path to upload images
     *
     * @var string
     */
    protected $pathUpload = '/upload/application/';

    /**
     * Set the value of the setting.
     *
     * @param $value
     * @return string
     */
    public function setValueAttribute($value)
    {
        if ($value === '' || is_null($value)) {
            return $this->attributes['value'] = '';
        }

        if (! $value instanceof UploadedFile) {
            return $this->attributes['value'] = $value;
        }

        return $this->uploadImage($value);
    }

    /**
     * Get the setting value.
     *
     * @param $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        if ($this->getAttribute('type') === 'image') {
            return $this->pathUpload . $value;
        }

        return $value;
    }

    /**
     * Upload the image to the server.
     *
     * @param $image
     * @return string
     */
    protected function uploadImage($image)
    {
        $oldImage = isset($this->attributes['value']) ? $this->attributes['value'] : null;

        if ($oldImage) {
            File::delete(public_path($oldImage));
        }

        $imageName = (str_random(15) . time()) . "." . $image->getClientOriginalExtension();
        $image->move(public_path() . $this->pathUpload, $imageName);

        $this->attributes['type'] = 'image';
        return $this->attributes['value'] = $imageName;
    }
}
