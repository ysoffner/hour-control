<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Rule Validators.
     * @var array
     */
    protected $validators = [
        'user_hour' => 'App\Validators\UserHourValidation@validate'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerValidator();
    }

    /**
     * Register the validators.
     */
    protected function registerValidator()
    {
        foreach ($this->validators as $name => $validator) {
            Validator::extend($name, $validator);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
