<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SettingsSystemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the settings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }

    /**
     * Determine whether the user can update the settings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }
}
