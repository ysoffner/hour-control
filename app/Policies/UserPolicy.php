<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->getAttribute('role') == 'admin';
    }

    /**
     * Determine whether the user can receive notifications.
     *
     * @param User $user
     * @return bool
     */
    public function receiveNotifications(User $user)
    {
        $role = $user->getAttribute('role');

        return $role == 'admin' || $role == 'manager' ? true : false;
    }
}
