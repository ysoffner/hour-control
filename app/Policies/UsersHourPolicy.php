<?php

namespace App\Policies;

use App\User;
use App\UsersHour;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsersHourPolicy
{
    use HandlesAuthorization;

    /**
     * Keep track of users' hours.
     *
     * @param User $user
     * @return bool
     */
    public function monitorControl(User $user)
    {
        $role = $user->getAttribute('role');

        return $role == 'admin' || $role == 'manager' ? true : false;
    }
}
