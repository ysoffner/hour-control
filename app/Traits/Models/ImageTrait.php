<?php

namespace App\Traits\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

trait ImageTrait
{
    /**
     * Image name with $pathUpload.
     *
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $this->pathUpload . $value;
    }

    /**
     * Image name without $pathUpload.
     *
     * @return mixed
     */
    public function getImageNoPathAttribute()
    {
        return str_replace($this->pathUpload, "", $this->getAttribute('image'));
    }

    /**
     * Upload image when image is set.
     *
     * @param $image
     * @return UploadedFile|string
     */
    public function setImageAttribute($image)
    {
        if ($image === null) {
            return $this->attributes['image'] = '';
        }

        if (! $image instanceof UploadedFile) {
            return $this->attributes['image'] = $image;
        }

        $imageName = (str_random(15) . time()) . "." . $image->getClientOriginalExtension();
        $image->move(public_path() . $this->pathUpload, $imageName);

        return $this->attributes['image'] = $imageName;
    }

    /**
     * Delete image from path.
     *
     * @return $this
     */
    public function deleteImage()
    {
        if ($this->defaultImage != $this->getAttribute('image_no_path')) {
            File::delete(public_path($this->getAttribute('image')));
        }

        return $this;
    }

    /**
     * Define default image in user image.
     *
     * @return $this
     */
    public function putDefaultImage()
    {
        $this->attributes['image'] = $this->getAttribute('image');

        return $this;
    }
}