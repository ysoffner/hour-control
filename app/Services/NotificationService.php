<?php

namespace App\Services;

use App\Factories\FirebaseFactory;

class NotificationService
{
    /**
     * @var FirebaseFactory
     */
    protected $firebaseFactory;

    /**
     * NotificationService constructor.
     *
     * @param FirebaseFactory $firebaseFactory
     */
    public function __construct(FirebaseFactory $firebaseFactory)
    {
        $this->firebaseFactory = $firebaseFactory;
    }

    /**
     * Send the notification to all users.
     *
     * @param $trans
     * @param array $arguments
     * @param null $icon
     * @return array
     */
    public function send($trans, Array $arguments = [], $icon = null)
    {
        return $this->firebaseFactory->push('notifications', [
            'trans' => $trans,
            'arguments' => $arguments,
            'icon' => ! $icon ? 'default' : $icon,
            'timestamp' => time()
        ]);
    }
}